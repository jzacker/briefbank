/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.ui.thread;

import com.zacker.briefbank.model.Bank;
import com.zacker.briefbank.model.BankRecord;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.scene.control.ListView;

/**
 *
 * This is a runnable which will take a ListView in the constructor and after
 * run() is called, update the ListView with the global bank. Everything will be
 * sorted and displayed according to the BankDisplayOptions object. setBank()
 * method.
 */
public class BankListViewRunnable implements Runnable {

    ListView<BankRecord> listView;

    StringProperty statusProperty;

    public BankListViewRunnable(ListView<BankRecord> listView, StringProperty statusProperty) {
        super();
        this.listView = listView;
        this.statusProperty = statusProperty;
    }

    @Override
    public void run() {
        Bank b = Bank.getGlobalBank();

        listView.setItems(FXCollections.observableArrayList(b.getRecords()));

        statusProperty.setValue("Records: " + b.size());

    }

}
