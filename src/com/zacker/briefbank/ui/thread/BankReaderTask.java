/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.ui.thread;

import com.zacker.briefbank.model.Bank;
import com.zacker.briefbank.model.BankRecord;
import com.zacker.briefbank.util.BankUtils;
import java.io.File;
import javafx.application.Platform;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

/**
 *
 * @author Jeremy
 */
public class BankReaderTask extends Task<Void> {

    File file;
    Stage stage;
    ListView<BankRecord> listView;
    StringProperty stringProperty;
    BankListViewRunnable runLater;

    public BankReaderTask(File file, BankListViewRunnable runLater) {
        super();
        this.file = file;
        this.runLater = runLater;
    }

    public BankReaderTask(File file, Stage stage, ListView<BankRecord> listView, StringProperty stringProperty) {
        super();
        this.file = file;
        this.stage = stage;
        this.listView = listView;
        this.stringProperty = stringProperty;
    }

    @Override
    protected Void call() throws Exception {
        Bank b = BankUtils.read(file);
        Bank.setGlobalBank(b);

        Platform.runLater(new BankListViewRunnable(listView, stringProperty));

        return null;
    }
}
