/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.ui.thread;

import com.zacker.briefbank.model.BankRecord;
import com.zacker.briefbank.util.BankConstants;
import java.io.File;
import java.util.Map;
import javafx.application.Application.Parameters;
import javafx.beans.property.StringProperty;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

/**
 *
 * @author Jeremy
 */
public class StartUpBankTask extends BankReaderTask {

    Parameters params;

    //this method should load the bank info from start up information.
    public StartUpBankTask(Stage stage, ListView<BankRecord> listView, StringProperty stringProperty, Parameters params) {
        super(null, stage, listView, stringProperty);
       
        this.params = params;
    }

    @Override
    protected Void call() throws Exception {
        //load up the start up paramaters and look for the bank.
        //if the file is bad or the bank isnt found, make an error.

        boolean successful = false;
        Map<String, String> map = params.getNamed();
        
        //first look to see if we should load from a directory
        String directory = map.get(BankConstants.BANK_FROM_DIRECTORY);
        System.out.println("Directory is " + directory);
        if (directory != null) {
            this.file = new File(directory);
            
            super.call();
        }

        return null;
    }

}
