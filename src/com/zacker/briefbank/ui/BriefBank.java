/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.ui;

import com.zacker.briefbank.model.BankRecord;
import com.zacker.briefbank.model.search.BankSearchCriteria;
import com.zacker.briefbank.model.sort.SortUtils;
import com.zacker.briefbank.ui.event.ClearEventHandler;
import com.zacker.briefbank.ui.event.EmailEventListener;
import com.zacker.briefbank.ui.event.ListViewEventHandler;
import com.zacker.briefbank.ui.event.OpenDirectoryEventHandler;
import com.zacker.briefbank.ui.event.OpenFileEventHandler;
import com.zacker.briefbank.ui.event.SaveFileEventHandler;
import com.zacker.briefbank.ui.event.SearchEventHandler;
import com.zacker.briefbank.ui.event.SortByChangeListener;
import com.zacker.briefbank.ui.renderer.BankListViewCellFactory;
import com.zacker.briefbank.ui.thread.StartUpBankTask;

import com.zacker.briefbank.util.BankUtils;
import java.util.Arrays;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author Jeremy Zacker
 */
public class BriefBank extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        //setUserAgentStylesheet(STYLESHEET_MODENA);
        //lets create a default labelFont.
        Font labelFont = Font.font("Arial", FontWeight.BOLD, 14);
        Font subLabelFont = Font.font("Arial", FontPosture.ITALIC, 14);

        //the border pane for the main window
        BorderPane borderPane = new BorderPane();

        //the border pane for the search are where the user types in their
        //search parameters
        BorderPane searchBorderPane = new BorderPane();

        //we build the admin buttons
        FlowPane adminFlowPane = new FlowPane();
        Button clearButton = new Button("Clear");
        Button openFileButton = new Button("Open File");
        Button openDirectoryButton = new Button("Open Directory");
        Button saveFileButton = new Button("Save Button");
        adminFlowPane.getChildren().addAll(clearButton, openFileButton, openDirectoryButton, saveFileButton);

        //the search button.
        Canvas canvas = new Canvas(50, 80);
        drawSearchIcon(canvas.getGraphicsContext2D(), labelFont);
        Button searchButton = new Button("", canvas);

        //the emailButton
        canvas = new Canvas(50, 80);
        drawEmailIcon(canvas.getGraphicsContext2D(), labelFont);
        Button emailButton = new Button("", canvas);

        AnchorPane searchButtonAnchorPane = new AnchorPane();
        searchButtonAnchorPane.setTopAnchor(searchButton, 0.0);
        searchButtonAnchorPane.setBottomAnchor(searchButton, 0.0);
        searchButtonAnchorPane.setLeftAnchor(searchButton, 0.0);
        searchButtonAnchorPane.setRightAnchor(searchButton, 5.0);
        searchButtonAnchorPane.getChildren().addAll(searchButton);

        AnchorPane feedbackButtonAnchorPane = new AnchorPane();
        feedbackButtonAnchorPane.setTopAnchor(emailButton, 0.0);
        feedbackButtonAnchorPane.setBottomAnchor(emailButton, 0.0);
        feedbackButtonAnchorPane.setLeftAnchor(emailButton, 0.0);
        feedbackButtonAnchorPane.setRightAnchor(emailButton, 5.0);
        feedbackButtonAnchorPane.getChildren().addAll(emailButton);

        BorderPane searchRightBorderPane = new BorderPane();
        searchRightBorderPane.setLeft(searchButtonAnchorPane);
        searchRightBorderPane.setRight(feedbackButtonAnchorPane);
        searchBorderPane.setRight(searchRightBorderPane);

        //the fields for name, date, tage, topic.
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10));

        //the second column should expand
        ColumnConstraints column1 = new ColumnConstraints();
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setHgrow(Priority.ALWAYS);
        gridPane.getColumnConstraints().addAll(column1, column2);

        //First row is the name row.
        Label nameLabel = new Label("File Name:");
        nameLabel.setFont(labelFont);
        TextField nameField = new TextField();
        Label nameFieldLabel = new Label("this is not case-sensitive; use '*' for wildcard (Ex: Brief*.docx)");
        nameFieldLabel.setFont(subLabelFont);
        gridPane.add(nameLabel, 0, 0);
        gridPane.setMargin(nameLabel, new Insets(0, 5, 0, 0));
        gridPane.add(nameField, 1, 0);
        gridPane.add(nameFieldLabel, 1, 1);
        Separator separator = new Separator();
        gridPane.add(separator, 0, 2, 2, 1);
        gridPane.setMargin(separator, new Insets(5, 0, 5, 0));

        //Second row is the topic row
        Label topicLabel = new Label("Topic:");
        topicLabel.setFont(labelFont);
        TextField topicField = new TextField();
        Label topicFieldLabel = new Label("use comma to seperate topics (Ex: Evidence, Miranda)");
        topicFieldLabel.setFont(subLabelFont);
        gridPane.add(topicLabel, 0, 3);
        gridPane.add(topicField, 1, 3);
        gridPane.add(topicFieldLabel, 1, 4);
        separator = new Separator();
        gridPane.add(separator, 0, 5, 2, 1);
        gridPane.setMargin(separator, new Insets(5, 0, 5, 0));

        //row 3 is the tags row
        Label tagsLabel = new Label("Tags:");
        tagsLabel.setFont(labelFont);
        TextField tagsField = new TextField();
        Label tagsFieldLabel = new Label("this is not case sensitive, use '*' for wildcard, seperate by commas (Ex: frye, confession*)");
        tagsFieldLabel.setFont(subLabelFont);
        gridPane.add(tagsLabel, 0, 6);
        gridPane.add(tagsField, 1, 6);
        gridPane.add(tagsFieldLabel, 1, 7);

        searchBorderPane.setCenter(gridPane);

        //add a sort by to the bottom
        FlowPane searchBottomPane = new FlowPane();
        Label sortByLabel = new Label("Sort By");
        sortByLabel.setFont(labelFont);
        ComboBox sortByComboBox = new ComboBox();

        sortByComboBox.getItems().addAll(Arrays.asList(SortUtils.SORT_BY_OPTIONS));
        sortByComboBox.getSelectionModel().selectFirst();
        sortByComboBox.setStyle("-fx-font: 14px \"Arial\"; -fx-font-weight: bold;");

        searchBottomPane.getChildren().add(sortByLabel);
        searchBottomPane.getChildren().add(sortByComboBox);
        searchBottomPane.setMargin(sortByLabel, new Insets(0, 10, 0, 10));

        searchBorderPane.setBottom(searchBottomPane);

        borderPane.setTop(searchBorderPane);

        ListView<BankRecord> listView = new ListView<BankRecord>();
        listView.setOnMouseClicked(new ListViewEventHandler());
        listView.setCellFactory(new BankListViewCellFactory());

        borderPane.setCenter(listView);

        //create a status label and bind the results to a string property that can
        //be updated by other threads.
        Label statusLabel = new Label("Records: 0");
        StringProperty statusProperty = new SimpleStringProperty();
        statusLabel.textProperty().bind(statusProperty);
        borderPane.setBottom(statusLabel);

        //now add the event listeners
        searchButton.setOnMouseClicked(new SearchEventHandler(stage,
                listView,
                statusProperty,
                searchBorderPane,
                adminFlowPane,
                nameField,
                topicField,
                tagsField,
                sortByComboBox));

        clearButton.setOnMouseClicked(new ClearEventHandler(listView, statusProperty));

        openFileButton.setOnMouseClicked(new OpenFileEventHandler(stage, listView, statusProperty));

        openDirectoryButton.setOnMouseClicked(new OpenDirectoryEventHandler(stage, listView, statusProperty));

        saveFileButton.setOnMouseClicked(new SaveFileEventHandler(stage, listView, statusProperty));

        emailButton.setOnMouseClicked(new EmailEventListener(stage, listView, statusProperty));

        sortByComboBox.getSelectionModel().selectedItemProperty().addListener(new SortByChangeListener(listView));

        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

        //set Stage boundaries to visible bounds of the main screen
        stage.setX(primaryScreenBounds.getWidth() / 4);
        stage.setY(primaryScreenBounds.getHeight() / 4);
        stage.setWidth(primaryScreenBounds.getWidth() / 2);
        stage.setHeight(primaryScreenBounds.getHeight() / 2);

        Scene scene = new Scene(borderPane);
        stage.setScene(scene);
        stage.setTitle("Maryland OPD - Juvenile Brief Bank");
        stage.show();

        //run a background task to populate the window.
        BankUtils.runTask(new StartUpBankTask(stage, listView, statusProperty, getParameters()));

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        for (int i = 0; i < args.length; i++) {
            System.out.println("Args are " + args[i]);
        }
        launch(args);
    }

    private void drawSearchIcon(GraphicsContext gc, Font font) {
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(5);

        gc.strokeOval(10, 10, 27, 27);
        gc.setLineCap(StrokeLineCap.ROUND);
        gc.strokeLine(35, 35, 48, 48);

        gc.setFont(font);

        gc.setTextAlign(TextAlignment.CENTER);
        gc.setTextBaseline(VPos.CENTER);
        gc.fillText("Search", 27, 70);

    }

    private void drawEmailIcon(GraphicsContext gc, Font font) {
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(5);

        gc.strokeRect(5, 9, 40, 38);

//gc.strokeOval(10, 10, 25, 25);
        //gc.setLineCap(StrokeLineCap.ROUND);
        gc.strokeLine(6, 10, 25, 24);
        gc.strokeLine(25, 24, 44, 10);

        gc.setFont(font);

        gc.setTextAlign(TextAlignment.CENTER);
        gc.setTextBaseline(VPos.CENTER);
        gc.fillText("Email", 25, 70);

    }
}
