/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.ui.event;

import com.zacker.briefbank.model.BankRecord;
import javafx.beans.property.StringProperty;
import javafx.event.EventHandler;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 *
 * @author Jeremy
 */
public abstract class BriefBankButtonEventHandler implements EventHandler<MouseEvent> {
    Stage stage;
    ListView<BankRecord> listView;
    StringProperty stringProperty;
    
    public BriefBankButtonEventHandler(Stage stage, ListView<BankRecord> listView, StringProperty stringProperty) {
        this.stage = stage;
        this.listView = listView;
        this.stringProperty = stringProperty;
    }
    
    @Override
    public void handle(MouseEvent event) {
        if (event.getButton() == MouseButton.PRIMARY
                && event.getClickCount() == 1) {
            click();
        }
    }
    
    abstract void click();
    
}
