/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.ui.event;

import com.zacker.briefbank.model.BankRecord;
import com.zacker.briefbank.ui.thread.BankReaderTask;
import com.zacker.briefbank.util.BankUtils;
import java.io.File;
import javafx.beans.property.StringProperty;
import javafx.scene.control.ListView;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

/**
 *
 * @author jerem
 */
public class OpenDirectoryEventHandler extends BriefBankButtonEventHandler {

    DirectoryChooser directoryChooser;

    public OpenDirectoryEventHandler(Stage stage, ListView<BankRecord> listView, StringProperty stringProperty) {
        super(stage, listView, stringProperty);
    }

    @Override
    void click() {
        if (directoryChooser == null) {
            directoryChooser = new DirectoryChooser();
        }

        File file = directoryChooser.showDialog(stage);
        if (file != null && file.isDirectory()) {
            //BankReaderTask task = new BankReaderTask(file,new BankListViewRunnable(listView, stringProperty));
            BankReaderTask task = new BankReaderTask(file, stage, listView, stringProperty);//new BankListViewRunnable(listView, stringProperty));
            BankUtils.runTask(task);
        }
    }

}
