/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.ui.event;

import com.zacker.briefbank.model.BankRecord;
import com.zacker.briefbank.ui.thread.BankReaderTask;
import com.zacker.briefbank.util.BankUtils;
import java.io.File;
import javafx.beans.property.StringProperty;
import javafx.scene.control.ListView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Jeremy
 */
public class OpenFileEventHandler extends BriefBankButtonEventHandler {

    FileChooser fileChooser;

    public OpenFileEventHandler(Stage stage, ListView<BankRecord> listView, StringProperty stringProperty) {
        super(stage, listView, stringProperty);
    }

    @Override
    public void click() {

        if (fileChooser == null) {
            fileChooser = new FileChooser();
            //Set extension filter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
            fileChooser.getExtensionFilters().add(extFilter);
        }
        
        File file = fileChooser.showOpenDialog(stage);
        if(file != null) {
            BankUtils.runTask(new BankReaderTask(file, stage, listView, stringProperty));
        }
    }

}
