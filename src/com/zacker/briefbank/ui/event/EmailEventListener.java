/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.ui.event;

import com.zacker.briefbank.model.BankRecord;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.StringProperty;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author jerem
 */
public class EmailEventListener extends BriefBankButtonEventHandler {

    Dialog<ButtonType> dialog;

    //create the radio buttons
    RadioButton emailSysAdmin;
    RadioButton submitButton;
    RadioButton forwardButton;

    public EmailEventListener(Stage stage, ListView<BankRecord> listView, StringProperty stringProperty) {
        super(stage, listView, stringProperty);
    }

    @Override
    void click() {
        if (dialog == null) {
            dialog = new Dialog<>();

            dialog.setTitle("E-mail");
            dialog.setHeaderText("What do you want to e-mail?");
            dialog.setResizable(true);

            //create the radio buttons
            emailSysAdmin = new RadioButton("Report an issue.");
            submitButton = new RadioButton("Submit a document to the brief bank.");
            forwardButton = new RadioButton("Forward the selected document to a friend.");

            ToggleGroup group = new ToggleGroup();
            emailSysAdmin.setToggleGroup(group);
            submitButton.setToggleGroup(group);
            forwardButton.setToggleGroup(group);

            emailSysAdmin.setSelected(true);
            VBox vbox = new VBox();
            vbox.setSpacing(10);
            vbox.getChildren().addAll(emailSysAdmin, submitButton, forwardButton);

            dialog.getDialogPane().setContent(vbox);

           
            dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        }
        
        Optional<ButtonType> result = dialog.showAndWait();
       
        if(result.get() == ButtonType.OK) {
            //do something.
            Desktop desktop = Desktop.getDesktop();
            try {
                desktop.mail(new URI("mailto:jzacker@opd.state.md.us"));
            } catch (URISyntaxException | IOException ex) {
                Logger.getLogger(EmailEventListener.class.getName()).log(Level.SEVERE, null, ex);
            }  
        } 
    }

}
