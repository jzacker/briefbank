/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.ui.event;

import com.zacker.briefbank.model.Bank;
import com.zacker.briefbank.model.BankRecord;
import com.zacker.briefbank.model.search.BankSearchAll;
import com.zacker.briefbank.model.search.BankSearchByCriteria;
import com.zacker.briefbank.model.search.BankSearchCriteria;
import com.zacker.briefbank.model.sort.SortUtils;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 *
 * @author jerem
 */
public class SearchEventHandler extends BriefBankButtonEventHandler {

    BorderPane borderPane;
    Node node;
    TextField nameField;
    TextField topicField;
    TextField tagsField;
    ComboBox sortByComboBox;

    boolean isNodeSet = false;

    public SearchEventHandler(
            Stage stage,
            ListView<BankRecord> listView,
            StringProperty stringProperty,
            BorderPane borderPane,
            Node node,
            TextField nameField,
            TextField topicField,
            TextField tagsField,
            ComboBox sortByComboBox) {
        super(stage, listView, stringProperty);
        this.borderPane = borderPane;
        this.node = node;
        this.nameField = nameField;
        this.topicField = topicField;
        this.tagsField = tagsField;
        this.sortByComboBox = sortByComboBox;
    }

    @Override
    public void handle(MouseEvent event) {
        if (event.getClickCount() == 2
                && event.getButton() == MouseButton.SECONDARY
                && event.isControlDown()) {
            //so to go into admin mode, we will set the border pane top
            //we can do it on this thread because we are on the UI thread
            if (isNodeSet) {
                borderPane.getChildren().remove(node);
            } else {
                borderPane.setTop(node);
            }

            isNodeSet = !isNodeSet;
        } else {
            super.handle(event);
        }
    }

    @Override
    void click() {
        //this is where you do the search work.
        Bank b = Bank.getGlobalBank();

        List<BankRecord> result = null;
        if (b != null) {
            result
                    = new BankSearchByCriteria().search(Bank.getGlobalBank().getRecords(),
                            new BankSearchCriteria(nameField.getText(), topicField.getText(), tagsField.getText()));
        } else {
            result = new ArrayList<>();
        }

        //now populate the list view
        SortUtils.populateAndSort(listView, sortByComboBox.getSelectionModel().getSelectedItem(), result);

    }

}
