/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.ui.event;

import com.zacker.briefbank.model.BankRecord;
import com.zacker.briefbank.model.sort.SortUtils;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ListView;

/**
 *
 * @author Jeremy
 */
public class SortByChangeListener implements ChangeListener {

    private final ListView<BankRecord> listView;
    
    public SortByChangeListener(ListView<BankRecord> listView) {
        this.listView = listView;
    }

    @Override
    public void changed(ObservableValue observable, Object oldValue, Object newValue) {
        if(!oldValue.equals(newValue)) {
            //you basically have to get the list from the list view,
            //then sort by the appropriate comparator. then reassign the list back.
            SortUtils.sort(listView, newValue);
        }
    }
    


}
