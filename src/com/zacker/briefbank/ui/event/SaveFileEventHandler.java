/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.ui.event;

import com.zacker.briefbank.model.Bank;
import com.zacker.briefbank.model.BankRecord;
import com.zacker.briefbank.util.BankUtils;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.StringProperty;
import javafx.scene.control.ListView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Jeremy
 */
public class SaveFileEventHandler extends BriefBankButtonEventHandler {

    FileChooser fileChooser;

    public SaveFileEventHandler(Stage stage, ListView<BankRecord> listView, StringProperty stringProperty) {
        super(stage, listView, stringProperty);
    }

    @Override
    void click() {
        if (fileChooser == null) {
            fileChooser = new FileChooser();
        }

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showSaveDialog(stage);

        if (file != null) {

            try {
                BankUtils.write(Bank.getGlobalBank(), file, Boolean.TRUE);
            } catch (Exception ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
