/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.ui.event;

import com.zacker.briefbank.model.Bank;
import com.zacker.briefbank.model.BankRecord;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Jeremy
 */
public class ListViewEventHandler implements EventHandler<MouseEvent> {

    @Override
    public void handle(MouseEvent event) {
        if (event.getClickCount() == 2
                && event.getButton() == MouseButton.PRIMARY) {
            ListView<BankRecord>  listView = (ListView<BankRecord>)event.getSource();
            BankRecord bankRecord = listView.getSelectionModel().getSelectedItem();
            StringBuilder path = new StringBuilder();
            path.append(Bank.getGlobalBank().getFullPath());
            path.append(File.separatorChar);
            
            //just get the first path.
            List<String> topicPath = bankRecord.getTopicPaths().get(0).getPath();
            for(String s: topicPath) {
                path.append(s);
                path.append(File.separatorChar);
            }
            
            path.append(bankRecord.getFullName());
            
            System.out.println(getClass() + " tring to open " + path);
            try {
                java.awt.Desktop.getDesktop().open(new File(path.toString()));
            } catch (IOException ex) {
                Logger.getLogger(ListViewEventHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
