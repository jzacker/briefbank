/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.ui.event;

import com.zacker.briefbank.model.BankRecord;
import java.util.ArrayList;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.scene.control.ListView;

/**
 *
 * @author Jeremy
 */
public class ClearEventHandler extends BriefBankButtonEventHandler {

    public ClearEventHandler(ListView<BankRecord>  listView, StringProperty stringProperty) {
        super(null, listView, stringProperty);
    }

    @Override
    void click() {
        listView.setItems(FXCollections.observableArrayList(new ArrayList<BankRecord>()));
    }
}
