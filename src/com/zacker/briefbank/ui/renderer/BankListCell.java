/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.ui.renderer;

import com.zacker.briefbank.model.BankRecord;
import com.zacker.briefbank.model.BankRecordTopicPath;
import com.zacker.briefbank.util.BankConstants;
import java.util.List;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * This is the rendered for every bank record displayed in the list.
 */
public class BankListCell extends ListCell<BankRecord> {

    VBox vbox = new VBox();
    //FlowPane topFlowPane = new FlowPane();
    GridPane topGridPane = new GridPane();
    Label dateLabel = new Label();
    Label titleLabel = new Label();
    Label categoryLabel = new Label();

    Label button;

    public BankListCell() {
        //topFlowPane.setHgap(5);
        
        //the top grd pane should always expand on therightmost label.
        ColumnConstraints column1 = new ColumnConstraints();
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setHgrow(Priority.ALWAYS);
        topGridPane.getColumnConstraints().addAll(column1, column2);
        topGridPane.add(dateLabel, 0, 0);
        topGridPane.add(titleLabel, 1, 0);
        
        Image image = new Image(getClass().getResourceAsStream("file_open.png"));
        ImageView imageView = new ImageView(image);

        imageView.setFitWidth(20);
        imageView.setFitHeight(20);
        button = new Label("", imageView);
        //topFlowPane.getChildren().addAll(dateLabel, titleLabel);
     

        vbox.getChildren().addAll(topGridPane, categoryLabel);
        dateLabel.setStyle("-fx-text-fill: gray;");
        titleLabel.setStyle("-fx-font: 16px \"Verdana\"; -fx-font-weight: bold;");
 
    }

    @Override
    protected void updateItem(BankRecord item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {
            dateLabel.setText(BankConstants.DISPLAY_DATE_FORMAT.format(item.getDate()));
            titleLabel.setText(item.getTitle());

            StringBuilder builder = new StringBuilder();

            List<BankRecordTopicPath> topicPathList = item.getTopicPaths();
            int topicPathListSize = topicPathList.size();
            
            System.out.println(item.getFullName() + " topicPaths = " + topicPathListSize);
           
            for (int i = 0; i < topicPathListSize; i++) {
                builder = addTopicPath(builder, topicPathList.get(i));

                //if not the last, append a carrsiage return.
                if (i < topicPathListSize - 1) {
                    builder.append("\n");
                }
            }

            categoryLabel.setText(builder.toString());

            setGraphic(vbox);

            builder.setLength(0);
        }
    }

    private StringBuilder addTopicPath(StringBuilder sb, BankRecordTopicPath topicPath) {
        if (sb == null) {
            sb = new StringBuilder();
        }

        List<String> l = topicPath.getPath();
        if (l != null) {
            int size = l.size();
            for (int i = 0; i < size; i++) {
                sb.append(l.get(i));
                
                if(i < size - 1) {
                    sb.append(" > ");
                }
            }
        }

        return sb;
    }
}
