/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.ui.renderer;

import com.zacker.briefbank.model.BankRecord;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 *
 * @author Jeremy
 */
public class BankListViewCellFactory implements Callback<ListView<BankRecord>, ListCell<BankRecord>> {

    @Override
    public ListCell<BankRecord> call(ListView<BankRecord> param) {
        return new BankListCell();
    }
}