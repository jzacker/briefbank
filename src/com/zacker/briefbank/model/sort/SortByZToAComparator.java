/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.model.sort;

import com.zacker.briefbank.model.BankRecord;
import java.util.Comparator;

/**
 *
 * @author Jeremy
 */
public class SortByZToAComparator implements Comparator<BankRecord> {

    @Override
    public int compare(BankRecord o1, BankRecord o2) {
        return o2.getTitle().compareTo(o1.getTitle());
    }
    
}
