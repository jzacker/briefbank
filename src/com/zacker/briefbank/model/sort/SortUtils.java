/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.model.sort;

import com.zacker.briefbank.model.BankRecord;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

/**
 *
 * @author jerem
 */
public class SortUtils {

    static public final String[] SORT_BY_OPTIONS
            = {
                "Relevance",
                "Date (Oldest to Newest)",
                "Date (Newest to Oldest)",
                "File Name (A -> Z)",
                "File Name (Z -> A)"
            };

    /**
     *
     * @param s
     * @return
     */
    static public Comparator<BankRecord> getComparator(Object s) {
        Comparator<BankRecord> c = new EmptyComparator();

        if (s.equals(SORT_BY_OPTIONS[1])) {
            c = new SortByDateNewestToOldestComparator();
        } else if (s.equals(SORT_BY_OPTIONS[2])) {
            c = new SortByDateOldestToNewestComparator();
        } else if (s.equals(SORT_BY_OPTIONS[3])) {
            c = new SortByAToZComparator();
        } else if (s.equals(SORT_BY_OPTIONS[4])) {
            c = new SortByZToAComparator();
        }

        return c;
    }

    static public void sort(ListView<BankRecord> listView, Object sortValue) {
        ObservableList<BankRecord> list = listView.getItems();
        Comparator<BankRecord> c = getComparator(sortValue);
        Collections.sort(list, c);
    }

    static public void populateAndSort(ListView<BankRecord> listView, Object sortValue, List<BankRecord> list) {
        ObservableList<BankRecord> temp = FXCollections.observableArrayList(list);
        Comparator<BankRecord> c = getComparator(sortValue);
        Collections.sort(temp, c);
        listView.setItems(temp);
    }
}
