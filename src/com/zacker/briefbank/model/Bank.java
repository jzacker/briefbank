/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * This class represent an entire brief bank. It includes the path to the bank
 * (absolute path, the date last updated, and al of the entries.It is intended
 * to be just a container class whose visual representation is done elsewhere.
 *
 * The constructors are private because the class in intended to be instantiated
 * from the file System for which the user should use BankReader and BankWriter.
 *
 * @author Jeremy Zacker
 */
@XmlRootElement(name = "bank")
@XmlAccessorType(XmlAccessType.FIELD)
public class Bank implements Serializable {

    //this is a global Bank variable that help keep track of the current bank.
    @XmlTransient
    private static Bank bank = null;

    //This is the canonical path to the file which contains the XML for the bank.
    //May be null as the path to the XML file is not part of the internal XML
    //data structure.
    private String fullPath;

    //contains the last time the bank was updated.
    private Date lastUpdate;

    //the list of records in the bank.
    @XmlElementWrapper(name = "records")
    @XmlElement(name = "record")
    private List<BankRecord> records;

    public Bank() {
        this("");
    }

    public Bank(String fullPath) {
        this.fullPath = fullPath;
    }

    public List<BankRecord> addRecord(BankRecord record) {
        if (records == null) {
            records = new ArrayList<>();
        }

        if (record != null) {
            records.add(record);
        }
        return records;
    }

    public List<BankRecord> getRecords() {
        if (records == null) {
            records = new ArrayList<>();
        }

        return records;
    }

    public int size() {
        return getRecords().size();
    }

    public String getFullPath() {
        return fullPath;
    }

    //organized this bank and all its records. looks for duplicate records and forces them together.
    public void organize() {
        //loop through the records and start placing them in a hash map.
        //Use the full name and size as the key because those two values together define uniqueness.
        //if you find to items which are the same, combine them.
        if (records == null) {
            records = new ArrayList<>();
        }
        Map<String, BankRecord> map = new Hashtable<String, BankRecord>();
        String key = null;

        for (BankRecord record : records) {
            key = record.getFullName() + record.getSize();
            BankRecord check = map.get(key);
            if (check == null) {
                map.put(key, record);
            } else {
                for (BankRecordTopicPath topicPath : record.getTopicPaths()) {
                    //TO-DO this line is broken.
                    check.addTopicPath(topicPath);
                }
                map.put(key, record);
            }
        }

        //when finished, reset the current records and refresh them all.
        records.clear();
        records.addAll(map.values());

        records.forEach((record) -> {
            record.organize();
        });
    }

    static public Bank getGlobalBank() {
        return bank;
    }

    static public void setGlobalBank(Bank b) {
        bank = b;
    }
}
