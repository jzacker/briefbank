/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.model;

import com.zacker.briefbank.util.BankConstants;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Jeremy Zacker
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BankRecord implements Serializable {
    //astatisdate for theempty constructor.
    static public Date DEFAULT_DATE = new Date(0L);
    
    //this is the name of the record. Think of it like a document
    //title. It does not need to be unique. But it should be a 
    //combination of date and a title.
    @XmlElement(required = true)
    private String fullName;

    //this is the size of the file. the files size and fullname are used to
    //determine uniqueness.
    @XmlElement(required = true)
    private long size;
    
    //this is the title of the document. It is parsed from the fullName
    @XmlTransient
    private String title;
    
    //this is the date the bank record was created. This will help
    //sort through older records. This is parsed from the fullname.
    @XmlTransient
    private Date date;

    //this hash value is calculated after the constructor and used to uniquely
    //identify the record.
    @XmlTransient
    private String hash;

    //Thisis the record description. This is likely to be manually
    //entered by an administrator. This can be empty.
    private String description;

    //This is a list of keywords or tags associated with the record.
    //This can be null or an empty list.  Its instantiation in an ordered
    //list because of the equals() method.
    @XmlElementWrapper(name = "tags")
    @XmlElement(name = "tag")
    private List<String> tags;

    //This is a list of the paths (or directories) the record sits under.
    //A single bank record ay sit under severaldirectory paths. This should 
    //be an ordered list for the equals method.
    @XmlElementWrapper(name = "topicPaths")
    @XmlElement(name = "topicPath")
    List<BankRecordTopicPath> topicPaths;

    public BankRecord() {
        this("", 0);
    }

    public BankRecord(String fullName, long size) {
        this.fullName = fullName;
        this.size = size;
        
        hash = fullName + size;
        
        organize();
    }
    
    @Override
    public int hashCode() {
        return (getFullName() + getSize()).hashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        boolean equals = false;
        if(o != null && o instanceof BankRecord) {
            BankRecord br = (BankRecord)o;
            equals = getFullName().equals(br.getFullName()) && (getSize() == br.getSize());
        }
        
        return equals;
    }

    public String getFullName() {
        return fullName;
    }
    
    public long getSize() {
        return size;
    }

    public String getHash() {
        return hash;
    }
    
    public String getTitle() {
        return title;
    }
    
    public Date getDate() {
        return date;
    }
    
    public List<String> addTag(String tag) {
        if (tags == null) {
            tags = new ArrayList<>();
        }
        tags.add(tag);
        return tags;
    }

    public List<String> addTags(String[] tags) {
        if (tags == null) {
            this.tags = new ArrayList<>();
        }
        for (String tag : tags) {
            List<String> addTag = addTag(tag);
        }
        return this.tags;
    }
    
    public void addTopicPath(BankRecordTopicPath categoryPath) {
        if(topicPaths == null) {
            topicPaths = new ArrayList<BankRecordTopicPath>();
        }
        
        topicPaths.add(categoryPath);
    }

    public List<BankRecordTopicPath> getTopicPaths() {
        if(topicPaths == null) {
            topicPaths = new ArrayList<BankRecordTopicPath>();
        }
        
        return topicPaths;
    }
    
    //this method will recalculate the date and title of the BankRecord
    //based on the fullname.
    void organize() {
        try {
            date = BankRecord.DEFAULT_DATE;
            title = fullName;
            
            String[] split = fullName.split(" ", 2);
            
            date = BankConstants.XML_DATE_FORMAT.parse(split[0]);
            title = split[1];
        } catch (ParseException ex) {
            //Logger.getLogger(BankRecord.class.getName()).log(Level.SEVERE, null, ex);
        }
 
    }
}
