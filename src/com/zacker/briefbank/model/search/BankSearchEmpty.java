/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.model.search;

import com.zacker.briefbank.model.BankRecord;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jeremy
 */
public class BankSearchEmpty implements BankSearch {

    @Override
    public List<BankRecord> search(List<BankRecord> records, BankSearchCriteria criteria) {
        return new ArrayList<>();
    }
    
}
