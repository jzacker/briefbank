/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.model.search;

import com.zacker.briefbank.model.BankRecord;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * This class ignores the search criteria, and returns all records in the bank. But it does
 * apply the comparator.
 */
public class BankSearchAll implements BankSearch {

    @Override
    public List<BankRecord> search(List<BankRecord> records, BankSearchCriteria criteria) {
        List<BankRecord> list = new ArrayList<>(records);

        return list;
    }
}
