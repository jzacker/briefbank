/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.model.search;

import com.zacker.briefbank.model.BankRecord;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author jerem
 */
public class BankSearchByCriteria implements BankSearch {

    @Override
    public List<BankRecord> search(List<BankRecord> records, BankSearchCriteria criteria) {
        List<BankRecord> result = new ArrayList<>();
        String name = criteria.getName();
        name = name == null ? "" : name.trim();

        if (!name.isEmpty()) {
            Pattern pattern = Pattern.compile(createRegexFromGlob(name), Pattern.CASE_INSENSITIVE);
            Matcher matcher = null;
            //iterate over the list and return any title thatconts name.
            //Make it caps-insensitive.
            for (BankRecord record : records) {
                //might wnt to use pattern and matcher, but will settle for something else.
                matcher = pattern.matcher(record.getTitle());
                if (matcher.matches()) {
                    result.add(record);
                }
            }
        }

        return result;
    }

    private String createRegexFromGlob(String glob) {
        glob = '*' + glob + '*';
        StringBuilder out = new StringBuilder("^");
        for (int i = 0; i < glob.length(); i++) {
            final char c = glob.charAt(i);
            switch (c) {
                case '*':
                    out.append(".*");
                    break;
                case '?':
                    out.append('.');
                    break;
                case '.':
                    out.append("\\.");
                    break;
                case '\\':
                    out.append("\\\\");
                    break;
                default:
                    out.append(c);
            }
        }

        
        return out.toString();
    }

    private boolean stringCompare(String stringToSearch, String search) {
        //this could probably be done with regex but its apain inthe ass.
        //check to see if there is an asterik in the string.
        //if there is an asterik, we do a more thorough search, otherwise its just a contains.
        boolean found = false;
        if (search.contains("*")) {
            String[] split = search.split("*");

        } else {
            found = stringToSearch.toLowerCase().contains(search.toLowerCase());
        }
        return found;
    }

}
