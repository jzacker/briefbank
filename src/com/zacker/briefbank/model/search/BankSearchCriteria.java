/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.model.search;

/**
 *
 * @author Jeremy
 */
public class BankSearchCriteria {
    private final String name;
    
    private final String topic; 
    
    private final String tags; 
    
    public BankSearchCriteria(String name, String topic, String tags) {
        this.name = name;
        this.topic = topic;
        this.tags = tags;
    }

    public BankSearchCriteria() {
        this("", "", "");
    }
    
    public String getName() {
        return name;
    }
    
    public String getTopic() {
        return topic;
    }
    
    public String getTags() {
        return tags;
    }
    
}
