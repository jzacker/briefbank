/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.model;

import com.zacker.briefbank.util.BankConstants;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 * This class merely provides a standard date format for the files and the XML.
 */
public class BankDateAdapter extends XmlAdapter<String, Date> {
    @Override
    public Date unmarshal(String s) throws Exception {
        synchronized (BankConstants.XML_DATE_FORMAT) {
            return BankConstants.XML_DATE_FORMAT.parse(s);
        }
    }

    @Override
    public String marshal(Date date) throws Exception {
        synchronized (BankConstants.XML_DATE_FORMAT) {
            return BankConstants.XML_DATE_FORMAT.format(date);
        }
    }

}
