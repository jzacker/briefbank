/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jeremy Zacker
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BankRecordTopicPath implements Serializable {
    
    private List<String> path;
    
    public BankRecordTopicPath() {
        
    }
    
    public List<String> addPath(String path) {
        if(this.path == null) {
            this.path = new ArrayList<>();
        }
        
        this.path.add(path);
        return this.path;
    }
    
    public void addPaths(Collection<String> paths) {
        if(path == null) {
            path = new ArrayList<>();
        }
        
        path.addAll(paths);
    }
    
    public List<String> getPath() {
        if(path == null) {
            path = new ArrayList<>();
        }
        
        return path;
    }
    
}
