package com.zacker.briefbank.util;

import com.zacker.briefbank.model.Bank;
import com.zacker.briefbank.model.BankRecord;
import com.zacker.briefbank.model.BankRecordTopicPath;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jeremy
 */
public class BankUtils {

    static public void runTask(Task<Void> task) {
        //run a background task to populate the window.
        ExecutorService executorService
                = Executors.newFixedThreadPool(1);
        executorService.execute(task);
        executorService.shutdown();
    }

    static public Bank createBankFromDirectory(File directory) {
        //this must be a directory

        Bank bank = new Bank(directory.getAbsolutePath());

        //now loop through the bank children and append to the bank.
        //the list we create is for the rectories we traverse.
        Deque<String> traverse = new LinkedList<String>();
        addBankRecord(bank, directory, traverse, false);
      
        bank.organize();

        return bank;
    }

    static public Bank read(File file) throws Exception {
        Bank bank = null;
        if (file.isFile()) {
            JAXBContext jaxbContext = JAXBContext.newInstance(Bank.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            bank = (Bank) jaxbUnmarshaller.unmarshal(file);

            //System.out.println("Read from file " + bank.getRecords().size());
            write(bank, System.out);
            
            bank.organize();
            
            //System.out.println("After ORganize " + bank.getRecords().size());
            
        } else if (file.isDirectory()) {
            bank = createBankFromDirectory(file);
        }

        return bank;
    }

    //take a bank object and write it to the output stream. 
    //Add boolean if you want the output stream formatted or not.
    static public void write(Bank bank, OutputStream out, Boolean format) throws Exception {
        JAXBContext jaxbContext = JAXBContext.newInstance(Bank.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, format);
        jaxbMarshaller.marshal(bank, out);
    }

    //take a bank object and write it to the output stream. 
    //Add boolean if you want the output stream formatted or not.
    static public void write(Bank bank, File file, Boolean format) throws Exception {
        JAXBContext jaxbContext = JAXBContext.newInstance(Bank.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, format);
        jaxbMarshaller.marshal(bank, file);
    }

    //take a bank object and write, FORMATTED, it to the output stream. 
    static public void write(Bank bank, OutputStream out) throws Exception {
        write(bank, out, Boolean.TRUE);
    }

    //this method takes the given bank and recursively gpoes through the file tree. 
    //@travers is an ordered list which stores the path followed, directory by directory.
    //@addtolist says whether to append the current directory to the traverse list. the REcursive nature
    //of the method required a boolean switch
    static private void addBankRecord(Bank bank, File file, Deque<String> traverse, boolean addToList) {
        if (file == null) {
            return;
        }
        if (file.isDirectory()) {
            if (addToList) {
                traverse.add(file.getName());
            }
            File[] children = file.listFiles();
            if (children != null) {
                for (File child : children) {
                    addBankRecord(bank, child, traverse, true);
                }
            }
            if (addToList && !traverse.isEmpty()) {
                traverse.removeLast();
            }
        } else {
            BankRecord bankRecord = new BankRecord(file.getName(), file.length());
            BankRecordTopicPath path = new BankRecordTopicPath();
            path.addPaths(traverse);
            bankRecord.addTopicPath(path);
            bank.addRecord(bankRecord);

        }

    }

    static public ImageView createFileIcon() {
        ImageView imageView = null;
        try {

            File file = File.createTempFile("icon", ".png");
            javax.swing.JFileChooser fc = new javax.swing.JFileChooser();
            javax.swing.Icon icon = fc.getUI().getFileView(fc).getIcon(file);

            BufferedImage bufferedImage = new BufferedImage(
                    icon.getIconWidth(),
                    icon.getIconHeight(),
                    BufferedImage.TYPE_INT_ARGB
            );
            icon.paintIcon(null, bufferedImage.getGraphics(), 0, 0);

            Image fxImage = SwingFXUtils.toFXImage(
                    bufferedImage, null
            );
            imageView = new ImageView(fxImage);

            return imageView;
        } catch (IOException ex) {
            Logger.getLogger(BankUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

        return imageView;
    }

}
