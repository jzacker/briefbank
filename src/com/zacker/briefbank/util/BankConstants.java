/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zacker.briefbank.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Jeremy
 */
public interface BankConstants {
    //this is the default date to use for a file if one cannot be determined.
    static public final Date DEFAULT_DATE = new Date(0L);
    
    //the his the date format to be used with file names and XML elements.
    static public final SimpleDateFormat XML_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    
    //thi is the date format for display.
    static public final SimpleDateFormat DISPLAY_DATE_FORMAT= new SimpleDateFormat("(yyyy)");
    
    //this is a command line argument which tells the program to
    //pull the bank from a particular file. The next argument should be the absolute
    //file path.
    static public final String BANK_FROM_XML_FILE = "BankXMLFile";
    
    //this tells us to grab the bank from a directory. It is mostly used
    //for testing where there is no XML file. The next argument should be the absolute
    //directory path.
    static public final String BANK_FROM_DIRECTORY = "BankFromDirectory";
    
    //this is the default file name for a bank.
    static public final String DEFAULT_BANK_XML_FILE_NAME = "bank.xml";
}
